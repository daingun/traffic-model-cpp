CXXFLAGS = --std=c++20 -Werror -Wpedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self -Wlogical-op -Wmissing-include-dirs -Wnoexcept -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-promo -Wstrict-null-sentinel -Wstrict-overflow=5 -Wundef -Wno-unused -Wno-variadic-macros -Wno-parentheses -fdiagnostics-show-option -Wconversion
NAME = traffic
OUTPUT = ./output

all: debug

debug: CXXFLAGS += -ggdb3
debug: executable

release: CXXFLAGS += -O2
release: executable

executable: $(NAME).cpp
	[ -d $(OUTPUT) ] || mkdir -p $(OUTPUT)
	$(CXX) $(CXXFLAGS) -o output/$(NAME) $(NAME).cpp

run:
	$(OUTPUT)/$(NAME)

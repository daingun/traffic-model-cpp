# Modello di Traffico Biham-Middleton-Levine

https://en.wikipedia.org/wiki/Biham%E2%80%93Middleton%E2%80%93Levine_traffic_model

https://www.jasondavies.com/bml/

## Uso - traffico
```
cmake --workflow --preset="rilascio-workflow"
./cmake_build/traffico
```

Il programma legge la configurazione iniziale dal file chiamato **problem.csv** (la configurazione descrive la posizione delle auto rosse e blu in una matrice). Questa configurazione rappresenta il passo 0.

La prima riga del file contiene una lista di numeri, separati dalla virgoal. Questi numeri sono i passi della simualzione per cui il programma deve scrivere un'uscita (un'uscita per ogni passo nella lista).

Le rimanenti righe descrivono lo stato iniziale della matrice per la simulazione (passo 0).
Ogni riga del file descrive una riga della tabella. Ogni riga del file contiene una lista di numeri separati dalla virgola. I numeri possono essere:
- 0: cella vuota
- 1: auto blue
- 2: auto rossa

L'uscita è rediretta in file. Il nome di questi file è "<numero\_passo>.csv".

Esempio di file di ingresso:
```
    1,10,100
    0,1,0,1,0
    2,0,2,0,0
    0,0,1,0,0
```

Esempio di file di uscita a passo uno ("1.cvs"):
```
    0,0,1,0,0
    2,1,2,1,0
    0,0,0,0,0
```

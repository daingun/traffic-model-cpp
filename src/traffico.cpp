#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <ranges>
#include <sstream>
#include <string>
#include <vector>

enum class Automobile { VUOTO = 0, BLU = 1, ROSSO = 2 };

using Matrice = std::vector<std::vector<Automobile>>;
using F = std::function<void(Matrice const&, Automobile, size_t, size_t, size_t,
                             Matrice&)>;

template <typename T>
bool sono_diversa_lunghezza(std::vector<std::vector<T>> const& v,
                            size_t const dimensione);

Automobile automobile_da_intero(int const intero);
int intero_da_automobile(Automobile const automobile);
bool è_cella_vuota(Automobile const cella);
void spingi_giù_blu(Matrice const& matrice, Automobile automobile, size_t riga,
                    size_t colonna, size_t righe, Matrice& tmp_matrice);
void spingi_destra_rosso(Matrice const& matrice, Automobile automobile,
                         size_t riga, size_t colonna, size_t colonne,
                         Matrice& tmp_matrice);

// Classe contenente le informazioni per l'algoritmo.
class Traffico
{
private:
        std::vector<int> passi;
        size_t righe{0};
        size_t colonne{0};
        Matrice matrice; // Matrice di interi contenente la disposizione delle
                         // automobili.

        void carica_dati(std::ifstream& file);
        void stampa_a_schermo(int passo) const;
        void stampa_in_file(int passo) const;
        Matrice spingi(F fun, size_t dimensione) const;

public:
        Traffico() noexcept(false);
        friend std::ostream& operator<<(std::ostream&, Traffico const&);
        void stampa_dati_iniziali() const;
        void inizia();
};

// Costruttore della classe contenente le informazioni del file di ingresso.
Traffico::Traffico() noexcept(false)
{
        std::string nome_file{"problem.csv"};
        std::ifstream file_ingresso(nome_file);
        if (!file_ingresso) {
                std::ostringstream uscita;
                uscita << "Unable to open " << nome_file << ".\n";
                throw std::runtime_error(uscita.str());
        } else {
                carica_dati(file_ingresso);
        }

        righe = matrice.size();
        colonne = matrice[0].size();

        if (sono_diversa_lunghezza(matrice, colonne)) {
                std::string const uscita("Errore di analisi. "
                                         "Diversa lunghezza delle colonne.\n");
                throw std::runtime_error(uscita);
        }
}

// Salva i dati trovati nel file di ingresso nella classe Traffico.
void Traffico::carica_dati(std::ifstream& file)
{
        std::string riga;
        getline(file, riga);
        std::istringstream prima_riga(riga);
        while (prima_riga) {
                int passo;
                // Prendi tutti i caratteri che possono essere considerati un
                // numero.
                prima_riga >> passo;
                passi.push_back(passo);
                prima_riga.get(); // Prendi e ignora la virgola.
        }

        while (getline(file, riga)) {
                std::istringstream record(riga);
                std::vector<Automobile> tmp;
                while (record) {
                        int intero;
                        // Prendi tutti i caratteri che possono essere
                        // considerati un numero.
                        record >> intero;
                        tmp.push_back(automobile_da_intero(intero));
                        record.get(); // Prendi e ignora la virgola.
                }
                matrice.push_back(tmp);
        }
}

// Definisci l'operatore << per la classe Traffico.
// È usabile sia per stampare a schermo e nei file.
std::ostream& operator<<(std::ostream& os, Traffico const& traffico)
{
        for (auto const& riga : traffico.matrice) {
                // Inizializza il separatore di colonna come stringa vuota.
                std::string separatore_colonna;
                for (auto const automobile : riga) {
                        os << separatore_colonna
                           << intero_da_automobile(automobile);
                        // Definisci il separatore solo dopo il primo ciclo.
                        separatore_colonna = ',';
                }
                os << '\n';
        }
        return os;
}

// Definisci il fomato di stampa a schermo.
void Traffico::stampa_a_schermo(int passo) const
{
        std::cout << "Passo: " << passo << '\n';
        std::cout << *this;
}

// Definisci il fomato di stampa nei file.
void Traffico::stampa_in_file(int passo) const
{
        std::ostringstream file;
        file << "output/" << passo << ".csv"; // Crea il nome del file.
        std::ofstream file_uscita(file.str());
        if (file_uscita) {
                file_uscita << *this;
        }
}

// Stampa i dati iniziali.
void Traffico::stampa_dati_iniziali() const
{
        for (auto const i : passi) {
                std::cout << i << ',';
        }
        std::cout << '\n';
        std::cout << righe << ", " << colonne << '\n';
}

// Ciclo principale dell'algoritmo.
void Traffico::inizia()
{
        if (passi.empty()) {
                return;
        }
        auto const ultimo_passo = passi.back();
        for (int passo : std::views::iota(1, ultimo_passo + 1)) {
                if (passo % 2 == 0) {
                        matrice = spingi(&spingi_destra_rosso, colonne);
                } else {
                        matrice = spingi(&spingi_giù_blu, righe);
                }
                auto stampa = std::ranges::any_of(
                    passi, [passo](int i) noexcept { return i == passo; });
                if (stampa) {
                        stampa_a_schermo(passo);
                        stampa_in_file(passo);
                }
        }
}

// Muovi l'automobile. Crea una nuova matrice di automobili.
Matrice Traffico::spingi(F fun, size_t dimensione) const
{
        Matrice tmp_matrice(
            righe, std::vector<Automobile>(colonne, Automobile::VUOTO));
        for (auto i{0}; auto const& riga : matrice) {
                for (auto j{0}; auto const automobile : riga) {
                        fun(matrice, automobile, i, j, dimensione, tmp_matrice);
                        ++j;
                }
                ++i;
        }
        return tmp_matrice;
}

// Spingi le automobili blu verso il basso.
void spingi_giù_blu(Matrice const& matrice, Automobile automobile, size_t riga,
                    size_t colonna, size_t righe, Matrice& tmp_matrice)
{
        switch (automobile) {
        case Automobile::BLU: {
                auto const nuova_riga = (riga + 1) % righe;
                if (è_cella_vuota(matrice[nuova_riga][colonna])) {
                        tmp_matrice[nuova_riga][colonna] = automobile;
                } else {
                        tmp_matrice[riga][colonna] = automobile;
                }
                break;
        }
        case Automobile::ROSSO: {
                tmp_matrice[riga][colonna] = automobile;
                break;
        }
        default:
                break;
        }
}

// Spingi le automobili rosse verso destra.
void spingi_destra_rosso(Matrice const& matrice, Automobile automobile,
                         size_t riga, size_t colonna, size_t colonne,
                         Matrice& tmp_matrice)
{
        switch (automobile) {
        case Automobile::ROSSO: {
                auto const nuova_colonna = (colonna + 1) % colonne;
                if (è_cella_vuota(matrice[riga][nuova_colonna])) {
                        tmp_matrice[riga][nuova_colonna] = automobile;
                } else {
                        tmp_matrice[riga][colonna] = automobile;
                }
                break;
        }
        case Automobile::BLU: {
                tmp_matrice[riga][colonna] = automobile;
                break;
        }
        default:
                break;
        }
}

// Controlla che una collezione di vettori abbiano tutti la stessa lunghezza.
template <typename T>
bool sono_diversa_lunghezza(std::vector<std::vector<T>> const& vec,
                            size_t const dimensione)
{
        return std::ranges::any_of(
            vec,
            // 'this' è catturata per riferimento automaticamente.
            [dimensione](auto r) noexcept { return r.size() != dimensione; });
}

// Controlla se una cella è vuota, non occupata da automobili rosse o blu.
bool è_cella_vuota(Automobile const cella)
{
        return cella == Automobile::VUOTO;
}

// Trasforma un intero in automobile.
Automobile automobile_da_intero(int const intero)
{
        switch (intero) {
        case 1: {
                return Automobile::BLU;
        }
        case 2: {
                return Automobile::ROSSO;
        }
        default:
                return Automobile::VUOTO;
        }
}

// Trasforma un'automobile in intero.
int intero_da_automobile(Automobile const automobile)
{
        switch (automobile) {
        case Automobile::BLU: {
                return 1;
        }
        case Automobile::ROSSO: {
                return 2;
        }
        default:
                return 0;
        }
}

int main()
{
        try {
                Traffico traffico;
                traffico.stampa_dati_iniziali();
                std::cout << traffico;
                traffico.inizia();
        } catch (std::runtime_error const& e) {
                std::cout << e.what() << std::endl;
                return 1;
        }

        return 0;
}
